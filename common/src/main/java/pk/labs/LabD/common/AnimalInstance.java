package pk.labs.LabD.common;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import org.osgi.framework.BundleContext;
import org.osgi.service.component.ComponentContext;
import pk.labs.LabD.contracts.Animal;
import pk.labs.LabD.contracts.Logger;

public class AnimalInstance implements Animal{
	
	//BundleContext bc
	//Map dict
	private BundleContext context;
	void activate(ComponentContext ctx){
        //context=bc;
        this.status=(String) ctx.getProperties().get("status");
        this.name=(String) ctx.getProperties().get("name");
        this.species=(String) ctx.getProperties().get("species");
        if(logger!=null)
        logger.get().log(this, species + " przybywa");  
    }    
        public AnimalInstance(){
            listeners= new PropertyChangeSupport(this);
        }
     
        
        private String species;
        private String name;
        private String status;
        private PropertyChangeSupport listeners;
        
        @Override
        public String getSpecies() {
            return species;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public String getStatus() {
            return status;
        }

        @Override
        public void setStatus(String status) {
                    String oldValue=this.status;
        this.status=status;
        listeners.firePropertyChange( new PropertyChangeEvent(this, "status", oldValue, status));
        }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        listeners.addPropertyChangeListener(listener);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        listeners.removePropertyChangeListener(listener);
    }
    private AtomicReference<Logger> logger = new AtomicReference<>();   
    void deactivate(){
         if(logger!=null)
            logger.get().log(this, species+" odchodzi");
    }

    void bindLogger(Logger logger) {
		this.logger.set(logger);
	}

	void unbindLogger(Logger logger) {
		this.logger.set(null);
	}
    }
