package pk.labs.LabD.actions.internal;

import pk.labs.LabD.contracts.Animal;
import pk.labs.LabD.contracts.AnimalAction;

public class Akcja1 implements AnimalAction
    {
      public String toString() {
        return "akcja 1";
      }
      public boolean execute(Animal animal)
      {
        animal.setStatus("akcja 1");
        return true;
      }
    };
