package pk.labs.LabD.actions.internal;

import pk.labs.LabD.contracts.Animal;
import pk.labs.LabD.contracts.AnimalAction;

public class Akcja3 implements AnimalAction
    {
      public String toString() {
        return "akcja 3";
      }

      public boolean execute(Animal animal)
      {
        animal.setStatus("akcja 3");
        return true;
      }
    };
