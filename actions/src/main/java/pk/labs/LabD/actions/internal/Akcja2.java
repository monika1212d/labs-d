package pk.labs.LabD.actions.internal;

import pk.labs.LabD.contracts.Animal;
import pk.labs.LabD.contracts.AnimalAction;

public class Akcja2 implements AnimalAction
    {
      public String toString() {
        return "akcja 2";
      }

      public boolean execute(Animal animal)
      {
        animal.setStatus("akcja 2");
        return true;
      }
    };
